const express = require('express');
// require our controllers.
const recordsController = require('../controllers/recordsController.js');

// recordRoutes is an instance of the express router
// use it to define our routes
// the router will be added as a middleware and will take control of requests starting with path /record
const recordsRoutes = express.Router();

/// RECORDS ROUTES ///

// get a list of all the records
recordsRoutes.get('/record', recordsController.getAllRecords);

// get a single record by id
recordsRoutes.get('/record/:id', recordsController.getRecordById);

// create a new record
recordsRoutes.post('/record/add', recordsController.createRecord);

// update a record by id
recordsRoutes.post('/update/:id', recordsController.updateRecordById);

// delete a record
recordsRoutes.delete('/:id', recordsController.deleteRecord);

module.exports = recordsRoutes;
