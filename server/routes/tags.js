const express = require('express');
// require our controllers.
const tagsController = require('../controllers/tagsController.js');

// tagsRoutes is an instance of the express router
// use it to define our routes
// the router will be added as a middleware and will take control of requests starting with path /tag
const tagsRoutes = express.Router();

/// TAGS ROUTES ///

// get a list of all the tags
tagsRoutes.get('/tag', tagsController.getAllTags);

// get a single tag by id
tagsRoutes.get('/tag/:id', tagsController.getTagById);

// create a new tag
tagsRoutes.post('/tag/add', tagsController.createTag);

// update a tag by id
tagsRoutes.post('/tag/update/:id', tagsController.updateTagById);

// delete a tag
tagsRoutes.delete('/tag/delete/:id', tagsController.deleteTag);

module.exports = tagsRoutes;
