// records controllers

// connect to the database
const dbo = require('../db/conn');

// convert the id from string to ObjectId for the _id
const ObjectId = require('mongodb').ObjectId;

// get a list of all the records
const getAllRecords = (req, res) => {
  let db_connect = dbo.getDb('employees');
  db_connect
    .collection('records')
    .find({})
    .toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
    });
};

// get a single record by id
const getRecordById = (req, res) => {
  let db_connect = dbo.getDb();
  let myquery = { _id: ObjectId(req.params.id) };
  db_connect.collection('records').findOne(myquery, function (err, result) {
    if (err) throw err;
    res.json(result);
  });
};

// create a new record
const createRecord = (req, response) => {
  let db_connect = dbo.getDb();
  let myobj = {
    taskName: req.body.taskName,
    description: req.body.description,
    tagId: req.body.tagId,
  };

  db_connect.collection('records').insertOne(myobj, function (err, res) {
    if (err) throw err;
    response.json(res);
  });
};

// update a record by id
const updateRecordById = (req, response) => {
  let db_connect = dbo.getDb();
  let myquery = { _id: ObjectId(req.params.id) };
  let newvalues = {
    $set: {
      taskName: req.body.taskName,
      description: req.body.description,
      tagId: req.body.tagId,
    },
  };
  db_connect.collection('records').updateOne(myquery, newvalues, function (err, res) {
    if (err) throw err;
    console.log('1 document updated');
    response.json(res);
  });
};

// delete a record
const deleteRecord = (req, response) => {
  let db_connect = dbo.getDb();
  let myquery = { _id: ObjectId(req.params.id) };
  db_connect.collection('records').deleteOne(myquery, function (err, obj) {
    if (err) throw err;
    console.log('1 document deleted');
    response.json(obj);
  });
};

module.exports = {
  getAllRecords,
  getRecordById,
  createRecord,
  updateRecordById,
  deleteRecord,
};
