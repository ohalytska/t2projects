// tags controllers

// connect to the database
const dbo = require('../db/conn');

// convert the id from string to ObjectId for the _id
const ObjectId = require('mongodb').ObjectId;

// get a list of all the tags
const getAllTags = (req, res, next) => {
  let db_connect = dbo.getDb('employees');
  db_connect
    .collection('tags')
    .find({})
    .toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      return next(result);
    });
};

// get a single tag by id
const getTagById = (req, res, next) => {
  let db_connect = dbo.getDb();
  let myquery = { _id: ObjectId(req.params.id) };
  db_connect.collection('tags').findOne(myquery, function (err, result) {
    if (err) throw err;
    res.json(result);
    return next(result);
  });
};

// create a new tag
const createTag = (req, res, next) => {
  let db_connect = dbo.getDb();
  let myobj = {
    tagName: req.body.tagName,
  };
  db_connect.collection('tags').insertOne(myobj, function (err, result) {
    if (err) throw err;
    res.json(result);
    return next(result);
  });
};

// update a tag by id
const updateTagById = (req, res, next) => {
  let db_connect = dbo.getDb();
  let myquery = { _id: ObjectId(req.params.id) };
  let newvalues = {
    $set: {
      tagName: req.body.tagName,
    },
  };
  db_connect.collection('tags').updateOne(myquery, newvalues, function (err, result) {
    if (err) throw err;
    console.log('1 document updated');
    res.json(result);
    return next(result);
  });
};

// delete a tag
const deleteTag = (req, res, next) => {
  let db_connect = dbo.getDb();
  let myquery = { _id: ObjectId(req.params.id) };
  db_connect.collection('tags').deleteOne(myquery, function (err, result) {
    if (err) throw err;
    console.log('1 document deleted');
    res.json(result);
    return next(result);
  });
};

module.exports = {
  getAllTags,
  getTagById,
  createTag,
  updateTagById,
  deleteTag,
};
