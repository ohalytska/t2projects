const express = require('express');
const app = express();
const cors = require('cors');
require('dotenv').config({ path: './config.env' });
const compression = require('compression');

// routes
const recordsRoutes = require('./routes/records.js');
const tagsRoutes = require('./routes/tags.js');

const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());
app.use(cors());
app.use(express.static('public'));
app.use(compression()); // compress all routes

// add routes
app.use(recordsRoutes);
app.use(tagsRoutes);

// get driver connection
const dbo = require('./db/conn');

app.listen(port, () => {
  // perform a database connection when server starts
  dbo.connectToServer(function (err) {
    if (err) {
      console.error(err);
    }
  });

  console.log(`Server is running on port: ${port}`);
});
