import React, { FC } from 'react';
import { Box, List, ListItemButton } from '@mui/material';
import { ITaskListProps } from './types';
import { TaskItem, boxStyle, TextHeader, EmptyList } from '..';
import { ITask } from '../../models';

export const TaskList: FC<ITaskListProps> = ({ toDoList, completeTask, editTask, tagList }: ITaskListProps) => {
  return (
    <Box sx={boxStyle}>
      <TextHeader text={'ToDo List'} />
      {toDoList.length > 0 ? (
        <List dense={true}>
          {toDoList.map((task: ITask) => (
            <ListItemButton key={task._id}>
              <TaskItem task={task} completeTask={completeTask} editTask={editTask} tagList={tagList} />
            </ListItemButton>
          ))}
        </List>
      ) : (
        <EmptyList />
      )}
    </Box>
  );
};
