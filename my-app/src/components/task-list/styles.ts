export const boxStyle = {
  bgcolor: 'background.paper',
  boxShadow: 1,
  borderRadius: 2,
  p: 2,
  width: '700px',
  minHeight: '300px',
  display: 'flex',
  justifyContent: 'flex-start',
  flexDirection: 'column',
  alignItems: 'flexStart',
};
