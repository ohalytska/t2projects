import { ITag, ITask } from '../../models';

export interface ITaskListProps {
  toDoList: ITask[];
  tagList: ITag[];
  completeTask: (_id: string) => Promise<void>;
  editTask: (handleClose: () => void, form: ITask) => Promise<void>;
}
