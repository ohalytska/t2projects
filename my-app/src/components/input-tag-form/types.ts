import { ChangeEvent } from 'react';

export interface IInputTagFormProps {
  tagName: string;
  tagError: boolean;
  addTag: () => Promise<void>;
  handleOnChange: (e: ChangeEvent<HTMLInputElement>) => void;
  setTagError: React.Dispatch<React.SetStateAction<boolean>>;
}
