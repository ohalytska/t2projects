import React, { FC, useEffect } from 'react';
import { Box, Button, TextField } from '@mui/material';
import { boxStyle } from './styles';
import { IInputTagFormProps } from '.';
import { TextHeader } from '..';

export const InputTagForm: FC<IInputTagFormProps> = ({
  tagName,
  handleOnChange,
  addTag,
  setTagError,
  tagError,
}: IInputTagFormProps) => {
  useEffect(() => {
    if (tagError && tagName.length > 0) {
      setTagError(false);
    }
  }, [tagName]);

  return (
    <Box sx={boxStyle}>
      <TextHeader text={'Add Tag'} />
      <TextField
        id="outlined-basic"
        label="Tag name"
        variant="outlined"
        placeholder="Enter tag name"
        name="tagName"
        value={tagName}
        onChange={handleOnChange}
        required
        error={tagError}
      />
      <Button variant="contained" onClick={addTag}>
        Add tag
      </Button>
    </Box>
  );
};
