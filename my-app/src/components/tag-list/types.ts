import { ITag } from '../../models';

export interface ITagListProps {
  tagList: ITag[];
  completeTag: (_id: string | undefined) => Promise<void>;
  editTag: (handleClose: () => void, form: ITag) => Promise<void>;
}
