import React, { FC } from 'react';
import { Box, List, ListItemButton } from '@mui/material';
import { ITagListProps } from './types';
import { TagItem, boxStyleTag, TextHeader, EmptyList } from '..';
import { ITag } from '../../models';

export const TagList: FC<ITagListProps> = ({ tagList, completeTag, editTag }: ITagListProps) => {
  return (
    <Box sx={boxStyleTag}>
      <TextHeader text={'Tag List'} />
      {tagList.length > 0 ? (
        <List dense={true} data-testid="tag-list">
          {tagList.map((tag: ITag) => (
            <ListItemButton key={tag._id}>
              <TagItem tag={tag} completeTag={completeTag} editTag={editTag} />
            </ListItemButton>
          ))}
        </List>
      ) : (
        <EmptyList />
      )}
    </Box>
  );
};
