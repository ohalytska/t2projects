import React, { FC } from 'react';
import { Typography, Divider, Box } from '@mui/material';
import { dividerStyle, ITextHeaderProps } from '.';

export const TextHeader: FC<ITextHeaderProps> = ({ text }: ITextHeaderProps) => {
  return (
    <Box>
      <Typography gutterBottom variant="h4" component="div">
        {text}
      </Typography>
      <Divider sx={dividerStyle} />
    </Box>
  );
};
