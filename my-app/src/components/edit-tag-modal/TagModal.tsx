import React, { ChangeEvent, FC, useState } from 'react';
import { Button, DialogActions, DialogContent, TextField } from '@mui/material';
import { ITagModalProps, tagDialogContent } from '.';
import { BootstrapDialog, BootstrapDialogTitle, textField } from '..';

export const TagModal: FC<ITagModalProps> = ({ handleClose, open, selectedTag, editTag }: ITagModalProps) => {
  const [form, setForm] = useState({
    _id: selectedTag._id,
    tagName: selectedTag.tagName,
  });

  const handleOnTextChange = (e: ChangeEvent<HTMLInputElement>): void => {
    if (e.target.name === 'tagName') {
      setForm({ ...form, tagName: e.target.value });
    }
  };

  return (
    <>
      <BootstrapDialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Update tag
        </BootstrapDialogTitle>
        <DialogContent dividers sx={tagDialogContent}>
          <TextField
            id="outlined-basic"
            label="Tag name"
            variant="outlined"
            placeholder="Enter tag name"
            name="tagName"
            value={form.tagName}
            onChange={handleOnTextChange}
            required
            sx={textField}
          />
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => editTag(handleClose, form)}>
            Save
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </>
  );
};
