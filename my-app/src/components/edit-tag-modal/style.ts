export const tagDialogContent = {
  display: 'flex',
  flexDirection: 'column',
  height: '150px',
  width: '400px',
  alignItems: 'center',
  justifyContent: 'space-around',
};
