import { ITag } from '../../models';

export interface ITagModalProps {
  open: boolean;
  handleClose: () => void;
  selectedTag: ITag;
  editTag: (handleClose: () => void, form: ITag) => Promise<void>;
}
