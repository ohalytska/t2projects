import { ITag, ITask } from '../../models';

export interface ISecondaryActionsProps {
  task: ITask;
  open: boolean;
  tagList: ITag[];
  completeTask: (_id: string) => Promise<void>;
  editTask: (handleClose: () => void, form: ITask) => Promise<void>;
  handleClose: () => void;
  handleOpen: () => void;
}
