import React, { FC } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { ISecondaryActionsProps } from '.';
import { IconButton } from '@mui/material';
import { TaskModal } from '..';

export const SecondaryActions: FC<ISecondaryActionsProps> = ({
  completeTask,
  task,
  handleOpen,
  handleClose,
  open,
  editTask,
  tagList,
}: ISecondaryActionsProps) => {
  return (
    <>
      <IconButton edge="end" aria-label="delete" onClick={() => completeTask(task._id)}>
        <DeleteIcon />
      </IconButton>
      <IconButton edge="end" aria-label="edit" onClick={handleOpen}>
        <EditIcon />
      </IconButton>
      <TaskModal open={open} handleClose={handleClose} selectedTask={task} editTask={editTask} tagList={tagList} />
    </>
  );
};
