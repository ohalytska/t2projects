import { SyntheticEvent } from 'react';
import { SNACKBAR } from '../../models';

export interface ICustomizedSnackbarProps {
  open: boolean;
  text: string;
  type: SNACKBAR;
  handleClose: (e?: SyntheticEvent | Event, reason?: string) => void;
}
