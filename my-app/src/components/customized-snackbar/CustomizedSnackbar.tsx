import React, { FC } from 'react';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import { Alert, alertStyle } from '..';
import { ICustomizedSnackbarProps } from './types';

export const CustomizedSnackbar: FC<ICustomizedSnackbarProps> = ({
  text,
  type,
  open,
  handleClose,
}: ICustomizedSnackbarProps) => {
  return (
    <Stack spacing={2} sx={alertStyle}>
      <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={type} sx={alertStyle}>
          {text}
        </Alert>
      </Snackbar>
    </Stack>
  );
};
