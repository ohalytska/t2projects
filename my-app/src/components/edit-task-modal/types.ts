import { ITag, ITask } from '../../models';

export interface ITaskModalProps {
  open: boolean;
  selectedTask: ITask;
  tagList: ITag[];
  editTask: (handleClose: () => void, form: ITask) => Promise<void>;
  handleClose: () => void;
}
