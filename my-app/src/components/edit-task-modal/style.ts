import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';

export const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

export const dialogContent = {
  display: 'flex',
  flexDirection: 'column',
  height: '300px',
  width: '400px',
  alignItems: 'center',
  justifyContent: 'space-around',
};

export const textField = {
  width: '100%',
};
