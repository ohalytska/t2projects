import React, { ChangeEvent, FC, useState } from 'react';
import { Button, DialogActions, DialogContent, TextField } from '@mui/material';
import { BootstrapDialog, dialogContent, ITaskModalProps, textField } from '.';
import { BootstrapDialogTitle } from '..';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import { ITask } from '../../models';

export const TaskModal: FC<ITaskModalProps> = ({
  handleClose,
  open,
  selectedTask,
  editTask,
  tagList,
}: ITaskModalProps) => {
  const [form, setForm] = useState<ITask>({
    _id: selectedTask._id,
    taskName: selectedTask.taskName,
    description: selectedTask.description,
    tagId: selectedTask?.tagId,
  });

  const [tagId, setTagId] = useState<string | undefined>(form?.tagId);

  const handleOnTextChange = (e: ChangeEvent<HTMLInputElement>): void => {
    if (e.target.name === 'task') {
      setForm({ ...form, taskName: e.target.value });
    } else if (e.target.name === 'description') {
      setForm({ ...form, description: e.target.value });
    }
  };

  const handleOnSelectChange = (e: SelectChangeEvent) => {
    const selectedTag = tagList.find((tag) => tag._id === e.target.value);
    if (selectedTag) {
      setTagId(selectedTag._id);
      setForm({ ...form, tagId: selectedTag._id });
    }
  };

  return (
    <>
      <BootstrapDialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Update task
        </BootstrapDialogTitle>
        <DialogContent dividers sx={dialogContent}>
          <TextField
            id="outlined-basic"
            label="Task name"
            variant="outlined"
            placeholder="Enter task name"
            name="task"
            value={form.taskName}
            onChange={handleOnTextChange}
            required
            sx={textField}
          />
          <TextField
            id="outlined-basic"
            label="Description"
            variant="outlined"
            placeholder="Enter task description"
            name="description"
            value={form.description}
            onChange={handleOnTextChange}
            sx={textField}
          />
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Tag</InputLabel>
            <Select
              labelId="select-tag"
              id="select-tag"
              value={tagId}
              name="tag"
              label="Tag"
              onChange={handleOnSelectChange}
            >
              {tagList.length === 0 ? (
                <MenuItem key={'No items'} disabled={true}>
                  {'No items'}
                </MenuItem>
              ) : (
                tagList.map((tag) => (
                  <MenuItem key={tag._id} value={tag._id}>
                    {tag.tagName}
                  </MenuItem>
                ))
              )}
            </Select>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => editTask(handleClose, form)}>
            Save
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </>
  );
};
