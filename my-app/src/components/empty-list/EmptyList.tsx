import { Typography } from '@mui/material';
import React, { FC } from 'react';
import { typographyStyle } from '.';

export const EmptyList: FC = () => {
  return (
    <Typography variant="subtitle1" gutterBottom component="div" sx={typographyStyle}>
      It`s kind of empty in here! Add something quickly!
    </Typography>
  );
};
