import React, { FC, ChangeEvent, useState, useCallback, useEffect, SyntheticEvent } from 'react';
import { Box } from '@mui/material';
import { InputTaskForm } from '../input-task-form';
import { TaskList } from '../task-list';
import { boxStyle } from './styles';
import { IForm, ITag, ITask, SNACKBAR } from '../../models';
import { CustomizedSnackbar } from '..';
import { SelectChangeEvent } from '@mui/material/Select';

export const ToDoList: FC = () => {
  const [form, setForm] = useState<IForm>({
    taskName: '',
    description: '',
    tagId: '',
  });
  const [toDoList, setToDoList] = useState<ITask[]>([]);
  const [openSnackbar, setOpenSnackbar] = useState<boolean>(false);
  const [snackbarType, setSnackbarType] = useState<SNACKBAR>(SNACKBAR.success);
  const [snackbarText, setSnackbarText] = useState<string>('');
  const [tagList, setTagList] = useState<ITag[]>([]);
  const [tag, setTag] = useState<ITag>();
  const [taskError, setTaskError] = useState<boolean>(false);

  const handleClose = (e?: SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handleOnTextChange = (e: ChangeEvent<HTMLInputElement>): void => {
    if (e.target.name === 'task') {
      setForm({ ...form, taskName: e.target.value });
    } else if (e.target.name === 'description') {
      setForm({ ...form, description: e.target.value });
    }
  };

  const handleOnSelectChange = (e: SelectChangeEvent) => {
    const selectedTag = tagList.find((tag) => tag._id === e.target.value);
    if (selectedTag) {
      setTag(selectedTag);
      setForm({ ...form, tagId: e.target.value });
    }
  };

  const getToDoList = useCallback(async () => {
    const response = await fetch(`http://localhost:5000/record/`);

    if (!response.ok) {
      const message = `An error occurred: ${response.statusText}`;
      setOpenSnackbar(true);
      setSnackbarText(message);
      setSnackbarType(SNACKBAR.error);
      return;
    }

    const toDoList = await response.json();
    setToDoList(toDoList);
  }, []);

  const addTask = async (): Promise<void> => {
    const newTask = { ...form };

    if (newTask.taskName.length === 0) {
      setTaskError(true);
      setOpenSnackbar(true);
      setSnackbarText('Please enter a task name!');
      setSnackbarType(SNACKBAR.error);
    } else {
      await fetch('http://localhost:5000/record/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newTask),
      }).catch((error) => {
        setOpenSnackbar(true);
        setSnackbarText(error);
        setSnackbarType(SNACKBAR.error);
        return;
      });

      setForm({
        taskName: '',
        description: '',
        tagId: '',
      });
      setTag(undefined);
      setOpenSnackbar(true);
      setSnackbarText('Task added!');
      setSnackbarType(SNACKBAR.success);
      getToDoList();
    }
  };

  const completeTask = async (_id: string): Promise<void> => {
    await fetch(`http://localhost:5000/${_id}`, {
      method: 'DELETE',
    });

    const newTasks = toDoList.filter((el) => el._id !== _id);
    setToDoList(newTasks);
    setOpenSnackbar(true);
    setSnackbarText('Task deleted!');
    setSnackbarType(SNACKBAR.info);
  };

  const editTask = async (handleClose: () => void, form: ITask) => {
    const editedTask: ITask = {
      _id: form._id,
      taskName: form.taskName,
      description: form.description,
      tagId: form.tagId,
    };

    await fetch(`http://localhost:5000/update/${form._id}`, {
      method: 'POST',
      body: JSON.stringify(editedTask),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    setOpenSnackbar(true);
    setSnackbarText('Task updated!');
    setSnackbarType(SNACKBAR.success);
    getToDoList();
    handleClose();
  };

  const getTagList = useCallback(async () => {
    const response = await fetch(`http://localhost:5000/tag/`);

    if (!response.ok) {
      const message = `An error occurred: ${response.statusText}`;
      setOpenSnackbar(true);
      setSnackbarText(message);
      setSnackbarType(SNACKBAR.error);
      return;
    }

    const tagList = await response.json();
    setTagList(tagList);
  }, []);

  useEffect(() => {
    getToDoList();
    getTagList();
  }, []);

  return (
    <>
      <Box sx={boxStyle}>
        <InputTaskForm
          task={form.taskName}
          description={form?.description}
          addTask={addTask}
          handleOnTextChange={handleOnTextChange}
          tag={tag}
          tagList={tagList}
          taskError={taskError}
          setTaskError={setTaskError}
          handleOnSelectChange={handleOnSelectChange}
        />
        <TaskList toDoList={toDoList} completeTask={completeTask} editTask={editTask} tagList={tagList} />
      </Box>
      <CustomizedSnackbar open={openSnackbar} text={snackbarText} type={snackbarType} handleClose={handleClose} />
    </>
  );
};
