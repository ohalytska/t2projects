export const boxStyle = {
  display: 'flex',
  justifyContent: 'space-evenly',
  bgcolor: '#f5f5f5',
  height: '100vh',
  padding: '20px 20px',
};
