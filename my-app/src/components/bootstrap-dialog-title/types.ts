export interface IDialogTitleProps {
  children?: React.ReactNode;
  onClose: () => void;
  id: string;
}
