import React, { FC } from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { dialogTitleStyle, buttonStyle, IDialogTitleProps } from '.';

export const BootstrapDialogTitle: FC<IDialogTitleProps> = ({ children, onClose }: IDialogTitleProps) => {
  return (
    <DialogTitle sx={dialogTitleStyle}>
      {children}
      {onClose ? (
        <IconButton aria-label="close" onClick={onClose} sx={buttonStyle}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};
