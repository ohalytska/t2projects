import { Theme } from '@mui/material';

export const dialogTitleStyle = { m: 0, p: 2 };

export const buttonStyle = {
  position: 'absolute',
  right: 8,
  top: 8,
  color: (theme: Theme) => theme.palette.grey[500],
};
