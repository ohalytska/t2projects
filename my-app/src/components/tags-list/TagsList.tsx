import React, { FC, ChangeEvent, useState, useCallback, useEffect, SyntheticEvent } from 'react';
import { Box } from '@mui/material';
import { InputTagForm } from '../input-tag-form';
import { ITag, SNACKBAR } from '../../models';
import { boxStyle } from '../to-do-list/styles';
import { CustomizedSnackbar, TagList } from '..';

export const TagsList: FC = () => {
  const [form, setForm] = useState({
    tagName: '',
  });
  const [tagList, setTagList] = useState<ITag[]>([]);
  const [openSnackbar, setOpenSnackbar] = useState<boolean>(false);
  const [snackbarType, setSnackbarType] = useState<SNACKBAR>(SNACKBAR.success);
  const [snackbarText, setSnackbarText] = useState<string>('');
  const [tagError, setTagError] = useState<boolean>(false);

  const handleClose = (e?: SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>): void => {
    if (e.target.name === 'tagName') {
      setForm({ ...form, tagName: e.target.value });
    }
  };

  const getTagList = useCallback(async () => {
    const response = await fetch(`http://localhost:5000/tag/`);

    if (!response.ok) {
      const message = `An error occurred: ${response.statusText}`;
      setOpenSnackbar(true);
      setSnackbarText(message);
      setSnackbarType(SNACKBAR.error);
      return;
    }

    const tagList = await response.json();
    setTagList(tagList);
  }, []);

  const addTag = async (): Promise<void> => {
    const newTag = { ...form };

    if (newTag.tagName.length === 0) {
      setTagError(true);
      setOpenSnackbar(true);
      setSnackbarText('Please enter a task name!');
      setSnackbarType(SNACKBAR.error);
    } else {
      await fetch('http://localhost:5000/tag/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newTag),
      }).catch((error) => {
        setOpenSnackbar(true);
        setSnackbarText(error);
        setSnackbarType(SNACKBAR.error);
        return;
      });

      setForm({
        tagName: '',
      });
      setOpenSnackbar(true);
      setSnackbarText('Tag added!');
      setSnackbarType(SNACKBAR.success);
      getTagList();
    }
  };

  const completeTag = async (_id?: string): Promise<void> => {
    if (_id) {
      await fetch(`http://localhost:5000/tag/delete/${_id}`, {
        method: 'DELETE',
      });

      const newTags = tagList.filter((el) => el._id !== _id);
      setTagList(newTags);
      setOpenSnackbar(true);
      setSnackbarText('Tag deleted!');
      setSnackbarType(SNACKBAR.info);
    }
  };

  const editTag = async (handleClose: () => void, form: ITag) => {
    const editedTag: ITag = {
      _id: form._id,
      tagName: form.tagName,
    };

    await fetch(`http://localhost:5000/tag/update/${form._id}`, {
      method: 'POST',
      body: JSON.stringify(editedTag),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    setOpenSnackbar(true);
    setSnackbarText('Tag updated!');
    setSnackbarType(SNACKBAR.success);
    getTagList();
    handleClose();
  };

  useEffect(() => {
    getTagList();
  }, []);

  return (
    <>
      <Box sx={boxStyle}>
        <InputTagForm
          tagName={form.tagName}
          addTag={addTag}
          handleOnChange={handleOnChange}
          tagError={tagError}
          setTagError={setTagError}
        />
        <TagList tagList={tagList} completeTag={completeTag} editTag={editTag} />
      </Box>
      <CustomizedSnackbar open={openSnackbar} text={snackbarText} type={snackbarType} handleClose={handleClose} />
    </>
  );
};
