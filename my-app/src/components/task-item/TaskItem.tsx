import React, { FC, useState } from 'react';
import { Chip, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import StarBorder from '@mui/icons-material/StarBorder';
import { ITaskItemProps } from '.';
import { SecondaryActions } from '..';

export const TaskItem: FC<ITaskItemProps> = ({ task, completeTask, editTask, tagList }: ITaskItemProps) => {
  const [open, setOpen] = useState<boolean>(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const selectedTagName = tagList.find((tag) => tag._id === task?.tagId)?.tagName;

  return (
    <ListItem
      secondaryAction={
        <SecondaryActions
          open={open}
          handleClose={handleClose}
          handleOpen={handleOpen}
          task={task}
          completeTask={completeTask}
          editTask={editTask}
          tagList={tagList}
        />
      }
    >
      <ListItemIcon>
        <StarBorder />
      </ListItemIcon>
      <ListItemText primary={task.taskName} />
      <ListItemText primary={task.description} />
      <ListItemText primary={<Chip label={selectedTagName} variant="outlined" />} />
    </ListItem>
  );
};
