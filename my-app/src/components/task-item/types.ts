import { ITask, ITag } from '../../models';

export interface ITaskItemProps {
  task: ITask;
  tagList: ITag[];
  completeTask: (_id: string) => Promise<void>;
  editTask: (handleClose: () => void, form: ITask) => Promise<void>;
}
