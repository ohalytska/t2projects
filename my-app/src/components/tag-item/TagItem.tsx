import React, { FC, useState } from 'react';
import { ListItem, ListItemIcon, ListItemText } from '@mui/material';
import StarBorder from '@mui/icons-material/StarBorder';
import { ITagItemProps } from '.';
import { SecondaryActionsTag } from '..';

export const TagItem: FC<ITagItemProps> = ({ tag, completeTag, editTag }: ITagItemProps) => {
  const [open, setOpen] = useState<boolean>(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <ListItem
      secondaryAction={
        <SecondaryActionsTag
          open={open}
          handleClose={handleClose}
          handleOpen={handleOpen}
          tag={tag}
          completeTag={completeTag}
          editTag={editTag}
        />
      }
    >
      <ListItemIcon>
        <StarBorder />
      </ListItemIcon>
      <ListItemText primary={tag.tagName} />
    </ListItem>
  );
};
