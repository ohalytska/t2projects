import { ITag } from '../../models';

export interface ISecondaryActionsTagProps {
  completeTag: (_id: string | undefined) => Promise<void>;
  editTag: (handleClose: () => void, form: ITag) => Promise<void>;
  tag: ITag;
  open: boolean;
  handleClose: () => void;
  handleOpen: () => void;
}
