import React, { FC } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { ISecondaryActionsTagProps } from '.';
import { IconButton } from '@mui/material';
import { TagModal } from '..';

export const SecondaryActionsTag: FC<ISecondaryActionsTagProps> = ({
  completeTag,
  tag,
  handleOpen,
  handleClose,
  open,
  editTag,
}: ISecondaryActionsTagProps) => {
  return (
    <>
      <IconButton edge="end" aria-label="delete" onClick={() => completeTag(tag._id)}>
        <DeleteIcon />
      </IconButton>
      <IconButton edge="end" aria-label="edit" onClick={handleOpen}>
        <EditIcon />
      </IconButton>
      <TagModal open={open} handleClose={handleClose} selectedTag={tag} editTag={editTag} />
    </>
  );
};
