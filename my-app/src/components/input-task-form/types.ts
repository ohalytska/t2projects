import { SelectChangeEvent } from '@mui/material';
import { ChangeEvent } from 'react';
import { ITag } from '../../models';

export interface IInputTaskFormProps {
  task: string;
  tag?: ITag;
  description?: string;
  tagList: ITag[];
  taskError: boolean;
  addTask: () => Promise<void>;
  handleOnTextChange: (e: ChangeEvent<HTMLInputElement>) => void;
  handleOnSelectChange: (e: SelectChangeEvent) => void;
  setTaskError: React.Dispatch<React.SetStateAction<boolean>>;
}
