import React, { FC, useEffect } from 'react';
import { Box, Button, TextField } from '@mui/material';
import { boxStyle } from './styles';
import { IInputTaskFormProps } from '.';
import { TextHeader } from '..';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export const InputTaskForm: FC<IInputTaskFormProps> = ({
  task,
  tag,
  description,
  handleOnTextChange,
  handleOnSelectChange,
  addTask,
  tagList,
  taskError,
  setTaskError,
}: IInputTaskFormProps) => {
  useEffect(() => {
    if (taskError && task.length > 0) {
      setTaskError(false);
    }
  }, [task]);

  return (
    <Box sx={boxStyle}>
      <TextHeader text={'Add Task'} />
      <TextField
        id="outlined-basic"
        label="Task name"
        variant="outlined"
        placeholder="Enter task name"
        name="task"
        value={task}
        onChange={handleOnTextChange}
        required
        error={taskError}
      />
      <TextField
        id="outlined-basic"
        label="Description"
        variant="outlined"
        placeholder="Enter task description"
        name="description"
        value={description}
        onChange={handleOnTextChange}
      />
      <FormControl fullWidth>
        <InputLabel id="select-tag">Tag</InputLabel>
        <Select
          labelId="select-tag"
          id="select-tag"
          value={tag?._id ?? ''}
          name="tag"
          label="Tag"
          onChange={handleOnSelectChange}
        >
          {tagList.length === 0 ? (
            <MenuItem key={'No items'} disabled={true}>
              {'No items'}
            </MenuItem>
          ) : (
            tagList.map((tag) => (
              <MenuItem key={tag._id} value={tag._id}>
                {tag.tagName}
              </MenuItem>
            ))
          )}
        </Select>
      </FormControl>
      <Button variant="contained" onClick={addTask}>
        Add task
      </Button>
    </Box>
  );
};
