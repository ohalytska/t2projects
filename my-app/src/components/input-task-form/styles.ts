export const boxStyle = {
  bgcolor: 'background.paper',
  boxShadow: 1,
  borderRadius: 2,
  p: 2,
  width: 500,
  height: 400,
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'column',
  alignItems: 'flexStart',
};
