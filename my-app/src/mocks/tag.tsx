import { ITag } from '../models';

export const tag: ITag = {
  _id: 'important',
  tagName: 'important',
};
