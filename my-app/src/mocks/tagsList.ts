import { ITag } from '../models';

export const tagsList: ITag[] = [
  {
    _id: 'important',
    tagName: 'important',
  },
  {
    _id: 'not important',
    tagName: 'not important',
  },
];
