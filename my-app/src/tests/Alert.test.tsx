import React from 'react';
import { render, screen } from '@testing-library/react';
import { Alert } from '../components';

test('is alert component work', () => {
  render(<Alert>{'Testing the alert window!'}</Alert>);

  const alertElement = screen.getByText(/Testing the alert window!/i);
  expect(alertElement).toBeInTheDocument();
});
