import React from 'react';
import { render } from '@testing-library/react';
import App from '../App';
import { createMemoryHistory } from 'history';
import { BrowserRouter } from 'react-router-dom';

test('renders the main page of app', () => {
  const history = createMemoryHistory();
  render(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
  );
  expect(history.location.pathname).toBe('/');
});
