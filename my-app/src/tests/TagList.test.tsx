import React from 'react';
import { TagList } from '../components';
import { render, screen } from '@testing-library/react';
import { tagsList } from '../mocks';

describe('TagList component', () => {
  test('is component header exist', () => {
    const completeTag = jest.fn();
    const editTag = jest.fn();
    render(<TagList tagList={[]} completeTag={completeTag} editTag={editTag} />);
    const tagListHeader = screen.getByText(/tag list/i);
    expect(tagListHeader).toBeInTheDocument();
  });

  test('when in component the tagList is empty exist', () => {
    const completeTag = jest.fn();
    const editTag = jest.fn();

    render(<TagList tagList={[]} completeTag={completeTag} editTag={editTag} />);

    const emptyList = screen.getByText(/it`s kind of empty in here! add something quickly!/i);
    expect(emptyList).toBeInTheDocument();
  });

  test('when in component the tagList has items', () => {
    const completeTag = jest.fn();
    const editTag = jest.fn();

    render(<TagList tagList={tagsList} completeTag={completeTag} editTag={editTag} />);

    const tagListItem = screen.getByTestId('tag-list');
    expect(tagListItem).toBeInTheDocument();
  });
});
