import React from 'react';
import { TagItem } from '../components';
import { fireEvent, render, screen } from '@testing-library/react';
import { tag } from '../mocks';

describe('TagItem component', () => {
  test('is component item exist', () => {
    const completeTag = jest.fn();
    const editTag = jest.fn();

    render(<TagItem tag={tag} completeTag={completeTag} editTag={editTag} />);

    const tagItem = screen.getByRole('listitem');
    expect(tagItem).toBeInTheDocument();
  });

  test('is the tag item displayed correctly', () => {
    const completeTag = jest.fn();
    const editTag = jest.fn();

    render(<TagItem tag={tag} completeTag={completeTag} editTag={editTag} />);

    const tagItemIcon = screen.getByTestId('StarBorderIcon');
    const tagName = screen.getByText(/important/i);
    const deleteButton = screen.getByLabelText('delete');
    const editButton = screen.getByLabelText('edit');

    expect(tagItemIcon).toBeInTheDocument();
    expect(tagName).toBeInTheDocument();
    expect(deleteButton).toBeInTheDocument();
    expect(editButton).toBeInTheDocument();
  });

  test('do the buttons in the tag item work', () => {
    const completeTag = jest.fn();
    const editTag = jest.fn();

    render(<TagItem tag={tag} completeTag={completeTag} editTag={editTag} />);

    const deleteButton = screen.getByLabelText('delete');
    const editButton = screen.getByLabelText('edit');

    fireEvent.click(deleteButton);
    expect(screen.queryByText(/it`s kind of empty in here! add something quickly!/i)).not.toBeInTheDocument();

    fireEvent.click(editButton);
    expect(screen.queryByRole('dialog'));
    const closeEditModal = screen.queryByLabelText('close');
    expect(closeEditModal).toBeInTheDocument();

    if (closeEditModal) {
      fireEvent.click(closeEditModal);
      expect(screen.queryByLabelText('customized-dialog-title')).not.toBeInTheDocument();
    }
  });
});
