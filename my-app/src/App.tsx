import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { TagsList, ToDoList } from './components';

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<ToDoList />} />
      <Route path="/tags" element={<TagsList />} />
      <Route path="*" element={<>No Page Found</>} />
    </Routes>
  );
};

export default App;
