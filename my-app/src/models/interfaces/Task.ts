export interface ITask {
  _id: string;
  taskName: string;
  description?: string;
  tagId?: string;
}
