export interface IForm {
  taskName: string;
  description?: string;
  tagId?: string;
}
