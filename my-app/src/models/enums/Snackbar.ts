export enum SNACKBAR {
  success = 'success',
  info = 'info',
  error = 'error',
}
